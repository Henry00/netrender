# BLENDER NETRENDER #

Originally we intended to use the netrender addon for Blender created by Martin Poirier (packed together with stable releases of Blender). However, it keeps crashing, has timeouts, various bugs, can only transfer exr files (often around 10 Mebibytes) and generally slaves continue to stop working after the slightest error.

We discovered that all that addon does is start another Blender and pass command line arguments, meaning the master and slaves don't even require an initial Blender to run. We are developing our own Master Server and Slave in the .NET Framework using C#.

### About this version of netrender ###

* Single multi-threaded program with GUI that can run multiple slaves and master servers at once.

![rand0404.png](https://bitbucket.org/repo/yG5zR5/images/674939711-rand0404.png)



* Simple Blender add-on to render your current scene on the network.

![rand0402.png](https://bitbucket.org/repo/yG5zR5/images/1354771249-rand0402.png)



* Designed with a remote connection through the internet in mind (e.g. ability to simply render and transfer PNG files only). Files are transferred as compressed zip files.
* The master server provides a webpage to follow all of the slaves and jobs live using ajax requests.

![rand0405.png](https://bitbucket.org/repo/yG5zR5/images/3400644747-rand0405.png)


* More details to come soon
* Please note that these screenshots are quite old from a WIP and may no longer accurately reflect the latest revision.