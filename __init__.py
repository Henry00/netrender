# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# This directory is a Python package.

bl_info = {
    "name": "Blender Network Renderer",
    "author": "Henry de Jongh, Georg Meier",
    "version": (1, 1),
    "blender": (2, 60, 0),
    "location": "Render > Engine > Blender Network Render",
    "description": "Distributed rendering for Blender using .NET",
    "category": "Render",
}

import bpy
from bpy.props import *
import os
import zipfile
import platform
import requests
 
class RENDER_PT_henry00_netrender_render(bpy.types.Operator):
    bl_label = "NetRender Render"
    bl_idname = "render.henry00_netrender_render"
    bl_description = "Render active scene on the network"
 
    def execute(self, context):
        # pack all of the files associated with this project.
        self.report({'INFO'}, "Packing all of the required files...")
        BlenderNetRenderCommon.pack_required_files(self)
        
        # send the zip file to the master server.
        BlenderNetRenderCommon.post_netrenderzip(self, context, False)
        
        return {'FINISHED'}
        
class RENDER_PT_henry00_netrender_animation(bpy.types.Operator):
    bl_label = "NetRender Animation"
    bl_idname = "render.henry00_netrender_animation"
    bl_description = "Render active scene animation on the network"
 
    def execute(self, context):
        # pack all of the files associated with this project.
        self.report({'INFO'}, "Packing all of the required files...")
        BlenderNetRenderCommon.pack_required_files(self)
        
        # send the zip file to the master server.
        BlenderNetRenderCommon.post_netrenderzip(self, context, True)
        
        return {'FINISHED'}
        
class BlenderNetRenderCommon(object):
    # pack all of the files associated with this project.
    @staticmethod
    def pack_required_files(self):
        zip_filepath = BlenderNetRenderCommon.zip_filepath(self)
        
        # remove old zip file.
        if os.path.exists(zip_filepath):
            os.remove(zip_filepath)
        
        # begin writing files into the zip file.
        with zipfile.ZipFile(zip_filepath, "w", zipfile.ZIP_DEFLATED) as zfile:
            ########################################################################################
            # the project .blend file.
            ########################################################################################
            filename = bpy.data.filepath
            # make sure the project file exists.
            if not os.path.exists(filename):
                self.report({'ERROR'}, "Please save your project before rendering on the network")
                return
            # save the project file first.
            else:
                bpy.ops.wm.save_mainfile(compress=True)
            # pack file.
            self.report({'INFO'}, "Packing all of the required files: Blender Project")
            zfile.write(filename, bpy.path.basename(filename))
            ########################################################################################
            
            ########################################################################################
            # the external resources.
            ########################################################################################
            for filename in bpy.utils.blend_paths():
                if os.path.exists(bpy.path.abspath(filename)):
                    # pack file.
                    self.report({'INFO'}, "Packing all of the required files: " + filename.strip('\\').strip('/'))
                    zfile.write(bpy.path.abspath(filename), filename.strip('\\').strip('/'))
                    
    # send the zip file to the master server.
    @staticmethod
    def post_netrenderzip(self, context, isanimation):
        zip_filepath = BlenderNetRenderCommon.zip_filepath(self)
        if isanimation:
            url = 'http://' + context.scene.henry00_netrender_server_address + ':' + str(context.scene.henry00_netrender_server_port) + '/job_' + str(context.scene.frame_start) + '&' + str(context.scene.frame_end) + '&' + context.scene.render.image_settings.file_format
        else:
            url = 'http://' + context.scene.henry00_netrender_server_address + ':' + str(context.scene.henry00_netrender_server_port) + '/job_' + str(context.scene.frame_current) + '&-1&' + context.scene.render.image_settings.file_format
        self.report({'INFO'}, "Uploading job to the master server...")
        
        # read the zip file
        try:
            data = open(zip_filepath, 'rb').read()
            # delete the zip file
            os.remove(zip_filepath)
        except:
            self.report({'ERROR'}, "Could not access the zip file")
            return
        
        # upload the zip file to the master server
        try:
            response = requests.post(url, data=data, headers={'Content-Type': 'application/octet-stream'})
            self.report({'INFO'}, "Finished, please check the website for job details")
        except:
            self.report({'ERROR'}, "Could not connect to the master server")
    
    # get the zip file path.
    @staticmethod
    def zip_filepath(self):
        return os.path.join(bpy.context.scene.render.filepath, "netrender.zip")
    
class BlenderNetRenderSettings(object):
    @staticmethod
    def load_properties():
        bpy.types.Scene.henry00_netrender_server_address = StringProperty(
                        name="Master",
                        description="IP or name of the master render server",
                        maxlen = 128,
                        default = "127.0.0.1")
        
        bpy.types.Scene.henry00_netrender_server_port = IntProperty(
                        name="Port",
                        description="port of the master render server",
                        default = 8000,
                        min=1,
                        max=65535)
    
    @staticmethod
    def render_panel(self, context):
        layout = self.layout

        # display render and animation buttons.
        row = layout.row(align=True)
        row.operator("render.henry00_netrender_render", text="Network Render", icon='RENDER_STILL')
        row.operator("render.henry00_netrender_animation", text="Network Animation", icon='RENDER_ANIMATION')
        
        # display master server and port fields.
        split = layout.split(percentage=0.65)
        split.column().prop(context.scene, "henry00_netrender_server_address")
        split.column().prop(context.scene, "henry00_netrender_server_port")
    
def register():
    bpy.utils.register_module(__name__)
    bpy.types.RENDER_PT_render.append(BlenderNetRenderSettings.render_panel)
    BlenderNetRenderSettings.load_properties()
 
def unregister():
    bpy.utils.unregister_module(__name__)
    
if __name__ == "__main__":
    register()