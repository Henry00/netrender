# NETRENDER - BLENDER #

Network Renderer (packed together with stable releases) by Martin Poirier http://wiki.blender.org/index.php/Dev:2.6/Source/Render/Cycles/Network_Render
Blender Network Render Additions https://github.com/WARP-LAB/Blender-Network-Render-Additions

### About this version of netrender ###

* The thumbnails are now the full, loss-less PNG compressed frames.
* More to come soon.