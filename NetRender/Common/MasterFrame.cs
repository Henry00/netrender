﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetRender.Common
{
    /// <summary>
    /// A frame state that is to be processed by slaves.
    /// </summary>
    public class MasterFrame
    {
        /// <summary>
        /// The frame has not been rendered yet.
        /// </summary>
        public const string STATUS_NEW = "New";
        /// <summary>
        /// The frame is currently being rendered.
        /// </summary>
        public const string STATUS_RENDERING = "Rendering";
        /// <summary>
        /// The frame has been rendered and is available on the master server.
        /// </summary>
        public const string STATUS_RENDERED = "Rendered";

        /// <summary>
        /// Whether this frame has been rendered and we have the file.
        /// </summary>
        public string Status = STATUS_NEW;
        /// <summary>
        /// The unique name of the slave that is assigned to this frame.
        /// </summary>
        public string SlaveName = "Noone";
    }
}
