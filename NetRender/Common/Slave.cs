﻿using NetRender.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetRender.Common
{
    /// <summary>
    /// Representation of a rendering slave (cannot instantiate the form many times, that'd be a waste of resources).
    /// </summary>
    public class Slave
    {
        /// <summary>
        /// Slave is idle and ready for an assignment.
        /// </summary>
        public const string STATUS_IDLE = "Idle";
        /// <summary>
        /// Slave received an assignment but isn't ready yet (i.e. missing files).
        /// </summary>
        public const string STATUS_STARTING = "Starting";
        /// <summary>
        /// Slave is currently rendering the assignment.
        /// </summary>
        public const string STATUS_RENDERING = "Rendering";
        /// <summary>
        /// Slave is currently uploading the output frames.
        /// </summary>
        public const string STATUS_UPLOADING = "Uploading";

        /// <summary>
        /// The unique name of the slave.
        /// </summary>
        public string Name = "INVALID";
        /// <summary>
        /// The name of the .blend file.
        /// </summary>
        public string BlendFile = "Unknown";
        /// <summary>
        /// The current status of the slave.
        /// </summary>
        public string Status = "INVALID";
        /// <summary>
        /// The name of the rendering job. job_00000000000000000000000000000000
        /// </summary>
        public string JobName = "job_00000000000000000000000000000000";
        /// <summary>
        /// The frame to start rendering at in Blender.
        /// </summary>
        public int FrameStart = 1;
        /// <summary>
        /// The frame to stop rendering at in Blender. -1 if it's a single frame.
        /// </summary>
        public int FrameEnd = -1;
        /// <summary>
        /// The output format the files should be in. TGA IRIS HAMX FTYPE JPEG MOVIE IRIZ RAWTGA AVIRAW AVIJPEG PNG BMP FRAMESERVER HDR TIFF EXR MPEG AVICODEC QUICKTIME CINEON DPX
        /// </summary>
        public string OutputFormat = "PNG";
        /// <summary>
        /// The time at which the slave last contacted the master server.
        /// </summary>
        public string LastSeen = DateTime.Now.ToString("hh\\:mm\\:ss");
        /// <summary>
        /// The amount of frames in this job.
        /// </summary>
        public int FrameCount
        {
            get { return FrameEnd == -1 ? 1 : FrameEnd - FrameStart; }
        }

        public Slave() { /* Serialization Constructor */ }
        public Slave(string _m_Name)
        {
            Name = _m_Name;
        }
    }
}
