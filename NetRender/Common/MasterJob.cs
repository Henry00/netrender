﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetRender.Common
{
    /// <summary>
    /// A job received from a client being processed on the master server.
    /// </summary>
    public class MasterJob
    {
        /// <summary>
        /// The name of the rendering job. job_00000000000000000000000000000000
        /// </summary>
        public string JobName = "job_00000000000000000000000000000000";
        /// <summary>
        /// The frame to start rendering at in Blender.
        /// </summary>
        public int FrameStart = 1;
        /// <summary>
        /// The frame to stop rendering at in Blender. -1 if it's a single frame.
        /// </summary>
        public int FrameEnd = -1;
        /// <summary>
        /// The output format the files should be in. TGA IRIS HAMX FTYPE JPEG MOVIE IRIZ RAWTGA AVIRAW AVIJPEG PNG BMP FRAMESERVER HDR TIFF EXR MPEG AVICODEC QUICKTIME CINEON DPX
        /// </summary>
        public string OutputFormat = "PNG";
        /// <summary>
        /// The date and time of when this job was started.
        /// </summary>
        public DateTime StartTime = DateTime.Now;
        /// <summary>
        /// Gets the lifetime this job has been active for.
        /// </summary>
        public string Lifetime
        {
            get
            {
                return (DateTime.Now - StartTime).ToString("dd\\.hh\\:mm\\:ss");
            }
        }
        /// <summary>
        /// The amount of frames in this job.
        /// </summary>
        public int FrameCount
        {
            get { return FrameEnd == -1 ? 1 : 1 + (FrameEnd - FrameStart); }
        }
        /// <summary>
        /// The amount of rendered frames in this job.
        /// </summary>
        public int FramesDoneCount
        {
            get {
                if (FrameEnd == -1)
                    if (Frames[0].Status == MasterFrame.STATUS_RENDERED)
                        return 1;
                    else
                        return 0;

                int c = 0;
                for (int i = FrameStart; i <= FrameEnd; i++)
                    if (Frames[i-1].Status == MasterFrame.STATUS_RENDERED)
                        c += 1;
                return c;
            }
        }
        /// <summary>
        /// The amount of dispatched (rendering) frames in this job.
        /// </summary>
        public int FramesDispatchedCount
        {
            get
            {
                if (FrameEnd == -1)
                    if (Frames[0].Status == MasterFrame.STATUS_RENDERING)
                        return 1;
                    else
                        return 0;

                int c = 0;
                for (int i = FrameStart; i <= FrameEnd; i++)
                    if (Frames[i-1].Status == MasterFrame.STATUS_RENDERING)
                        c += 1;
                return c;
            }
        }
        /// <summary>
        /// The frame states that are to be processed by slaves.
        /// </summary>
        public MasterFrame[] Frames;
        /// <summary>
        /// Gets a start and end position of unrendered frames.
        /// </summary>
        /// <param name="start">The start frame to begin rendering at.</param>
        /// <param name="end">The end frame to stop rendering at.</param>
        /// <param name="current_frame">The real start frame to render a single blender frame.</param>
        /// <returns>True if there are frames, False otherwise.</returns>
        public bool GetUnrenderedFrames(out int start, out int end, int current_frame)
        {
            start = -1;
            end = 0;

            /////////////////////////////////////////////////////////////////////////////////////////
            // SINGLE FRAME. Render only this one frame if it's not been done.
            /////////////////////////////////////////////////////////////////////////////////////////
            if (FrameEnd == -1)
                // frame has not been rendered yet, render it.
                if (Frames[0].Status == MasterFrame.STATUS_NEW)
                {
                    start = current_frame;
                    end = -1;
                    return true;
                }
                // frame was rendered already, no more work left.
                else
                    return false;

            /////////////////////////////////////////////////////////////////////////////////////////
            // MULTIPLE FRAMES.
            /////////////////////////////////////////////////////////////////////////////////////////
            // iterate through all of the frames in this job:
            for (int i = FrameStart; i <= FrameEnd; i++)
            {
                // frame has not been rendered yet.
                if (Frames[i - 1].Status == MasterFrame.STATUS_NEW)
                {
                    // is this the first unrendered frame we found?
                    if (start == -1)
                        // set this frame as the start position.
                        start = i;
                    else
                        // add additional frames to the end position.
                        end += 1;

                    // exceeded maximum amount of frames.
                    if (end >= 4)
                        break;
                }
                // frame has already been rendered.
                else
                {
                    // have we already found an unrendered frame?
                    if (start != -1)
                        // handle the unrendered frames we found.
                        break;
                }
            }
            // calculate end frame position.
            end = start + end;

            // check if it's only a single frame.
            if (start == end)
                // mark it as a single frame for external functions.
                end = -1;

            // there are no unrendered frames in this job.
            if (start == -1)
                return false;
            else
                return true;
        }

        public MasterJob(string _m_JobName, int _m_FrameStart, int _m_FrameEnd, string _m_OutputFormat, DateTime _m_StartTime)
        {
            JobName = _m_JobName;
            FrameStart = _m_FrameStart;
            FrameEnd = _m_FrameEnd;
            OutputFormat = _m_OutputFormat;
            StartTime = _m_StartTime;
            Frames = new MasterFrame[FrameCount];
            // instantiate each frame in the array.
            for (int i = 0; i < Frames.Length; i++)
                Frames[i] = new MasterFrame();
        }
    }
}
