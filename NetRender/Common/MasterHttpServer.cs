﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Threading;
using System.Net;
using System.IO;

namespace NetRender.Common
{
    class MasterHttpServer
    {
        /// <summary>
        /// The thread the webserver will run on as to not freeze the main thread.
        /// </summary>
        private Thread m_Thread;

        /// <summary>
        /// The HTTP listener as provided by the framework.
        /// </summary>
        private HttpListener m_Listener;

        /// <summary>
        /// The port that is being listened on.
        /// </summary>
        private int m_Port;

        /// <summary>
        /// The address to listen on.
        /// </summary>
        private string m_Address;

        /// <summary>
        /// Constructs a new http server for the master server.
        /// </summary>
        /// <param name="address">The address to listen on.</param>
        /// <param name="port">The port that is being listened on.</param>
        public MasterHttpServer(string address, int port)
        {
            m_Address = address;
            m_Port = port;
        }

        /// <summary>
        /// The main loop of the http server. Handles incoming requests.
        /// </summary>
        private void Listen()
        {
            m_Listener = new HttpListener();
            m_Listener.Prefixes.Add("http://*:" + m_Port.ToString() + "/");
            try
            {
                m_Listener.Start();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("This program must be run as an administrator." + Environment.NewLine + Environment.NewLine + ex.Message, "Blender NetRender Exception", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Stop);
            }

            for(;;)
            {
                // Waits for an incoming request and calls ProcessRequest once one is received.
                try
                {
                    _ProcessRequest(m_Listener.GetContext());
                }
                catch (Exception) { /*Unexplained HttpListenerException*/ }
            }
        }

        /// <summary>
        /// Start listening on the address and port on a new thread.
        /// </summary>
        public void StartListening()
        {
            m_Thread = new Thread(Listen);
            m_Thread.Start();
        }

        /// <summary>
        /// Stop listening and abort all activity.
        /// </summary>
        public void StopListening()
        {
            m_Thread.Abort();
            m_Listener.Stop();
        }

        /// <summary>
        /// Process a request.
        /// </summary>
        /// <param name="context">Additional request information.</param>
        private void _ProcessRequest(HttpListenerContext context)
        {
            if (ProcessRequest != null)
                ProcessRequest(this, new ProcessRequestEventArgs(context));
        }

        /// <summary>
        /// Event Arguments for the Process Request event.
        /// </summary>
        public class ProcessRequestEventArgs
        {
            private HttpListenerContext m_Context;

            public ProcessRequestEventArgs(HttpListenerContext context)
            {
                m_Context = context;
            }

            public HttpListenerContext Context
            {
                get { return m_Context; }
            }
        }

        public delegate void ProcessRequestHandler(object sender, ProcessRequestEventArgs e);
        public event ProcessRequestHandler ProcessRequest;
    }
}
