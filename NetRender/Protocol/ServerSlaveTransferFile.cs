﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetRender.Protocol
{
    /// <summary>
    /// Server -> Client. Transfer a file to the slave.
    /// </summary>
    [Serializable]
    public class ServerSlaveTransferFile
    {
        /// <summary>
        /// The name of the rendering job. job_00000000000000000000000000000000
        /// </summary>
        public string JobName = "job_00000000000000000000000000000000";
        /// <summary>
        /// The requested file.
        /// "job_zip" for the .blend and its external resources.
        /// </summary>
        public string FileName = "job_zip";
        /// <summary>
        /// The bytes of the file.
        /// </summary>
        public byte[] FileBytes = new byte[] { };

        public ServerSlaveTransferFile() { /* Serialization Constructor */ }
    }
}
