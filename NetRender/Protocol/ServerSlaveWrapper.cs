﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetRender.Protocol
{
    /// <summary>
    /// A wrapper class for all possible protocol packets to add an unknown type.
    /// </summary>
    [Serializable]
    public class ServerSlaveWrapper
    {
        /// <summary>
        /// Empty task that does nothing.
        /// </summary>
        public const string TYPE_NONE = "None";
        /// <summary>
        /// Start a new rendering job on the slave.
        /// </summary>
        public const string TYPE_RENDERJOB = "ServerSlaveRenderJob";
        /// <summary>
        /// Transfers a file to the slave.
        /// </summary>
        public const string TYPE_TRANSFERFILE = "ServerSlaveTransferFile";

        /// <summary>
        /// The type of protocol packet. ServerSlaveRenderJob, ServerSlaveTransferFile.
        /// </summary>
        public string Type = "INVALID";
        /// <summary>
        /// A render job from the master server.
        /// </summary>
        public ServerSlaveRenderJob RenderJob = null;
        /// <summary>
        /// A transfer file from the master server.
        /// </summary>
        public ServerSlaveTransferFile TransferFile = null;
    }
}
