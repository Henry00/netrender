﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetRender.Protocol
{
    /// <summary>
    /// Slave -> Master Server. Request a file (like the zip file) for a job.
    /// </summary>
    [Serializable]
    public class SlaveServerRequestFile
    {
        public const string FILE_JOB_ZIP = "job_zip";

        /// <summary>
        /// The name of the rendering job. job_00000000000000000000000000000000
        /// </summary>
        public string JobName = "job_00000000000000000000000000000000";
        /// <summary>
        /// The requested file.
        /// "job_zip" for the .blend and its external resources.
        /// </summary>
        public string FileName = FILE_JOB_ZIP;

        public SlaveServerRequestFile() { /* Serialization Constructor */ }
    }
}
