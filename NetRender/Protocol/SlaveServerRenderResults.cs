﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetRender.Protocol
{
    /// <summary>
    /// Slave -> Master Server. Sends the render results back to the master server.
    /// </summary>
    [Serializable]
    public class SlaveServerRenderResults
    {
        /// <summary>
        /// The name of the rendering job. job_00000000000000000000000000000000
        /// </summary>
        public string JobName = "job_00000000000000000000000000000000";
        /// <summary>
        /// The frame to start rendering at in Blender.
        /// </summary>
        public int FrameStart = 1;
        /// <summary>
        /// The frame to stop rendering at in Blender. -1 if it's a single frame.
        /// </summary>
        public int FrameEnd = -1;
        /// <summary>
        /// The bytes of the file.
        /// </summary>
        public byte[] FileBytes = new byte[] { };

        public SlaveServerRenderResults() { /* Serialization Constructor */ }
    }
}
