﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetRender.Protocol
{
    /// <summary>
    /// Master Server -> Slave. Start a new rendering job on the slave.
    /// </summary>
    [Serializable]
    public class ServerSlaveRenderJob
    {
        /// <summary>
        /// The name of the rendering job. job_00000000000000000000000000000000
        /// </summary>
        public string JobName = "job_00000000000000000000000000000000";
        /// <summary>
        /// The frame to start rendering at in Blender.
        /// </summary>
        public int FrameStart = 1;
        /// <summary>
        /// The frame to stop rendering at in Blender. -1 if it's a single frame.
        /// </summary>
        public int FrameEnd = -1;
        /// <summary>
        /// The output format the files should be in. TGA IRIS HAMX FTYPE JPEG MOVIE IRIZ RAWTGA AVIRAW AVIJPEG PNG BMP FRAMESERVER HDR TIFF EXR MPEG AVICODEC QUICKTIME CINEON DPX
        /// </summary>
        public string OutputFormat = "PNG";
        /// <summary>
        /// The amount of frames in this job.
        /// </summary>
        public int FrameCount
        {
            get { return FrameEnd == -1 ? 1 : FrameEnd - FrameStart; }
        }

        public ServerSlaveRenderJob() { /* Serialization Constructor */ }
        public ServerSlaveRenderJob(string _JobName, int _FrameStart, int _FrameEnd, string _OutputFormat)
        {
            JobName = _JobName;
            FrameStart = _FrameStart;
            FrameEnd = _FrameEnd;
            OutputFormat = _OutputFormat;
        }
    }
}
