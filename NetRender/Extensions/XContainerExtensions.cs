﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace NetRender.Extensions
{
    public static class XContainerExtensions
    {
        public static XElement AddGet(this XContainer container, XElement content)
        {
            container.Add(content);
            return content;
        }

        /// <summary>
        /// Adds an element if it does not yet exist.
        /// </summary>
        public static XElement AddDefault(this XContainer container, string name, XElement defaultvalue)
        {
            XElement element = container.Element(name);
            if (element == null)
                return container.AddGet(defaultvalue);
            return element;
        }
    }
}
