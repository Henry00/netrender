﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetRender.Extensions
{
    public static class StreamExtensions
    {
        public static void WriteString(this Stream stream, string text)
        {
            byte[] utf8 = Encoding.UTF8.GetBytes(text);
            stream.Write(utf8, 0, utf8.Length);
        }
    }
}
