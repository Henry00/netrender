﻿namespace NetRender
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.MainMenu_FileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMenu_FileMenu_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMenu_ViewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMenu_ViewMenu_Master = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMenu_ViewMenu_Slave = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMenu_ViewMenu_Seperator1 = new System.Windows.Forms.ToolStripSeparator();
            this.MainMenu_ViewMenu_Preferences = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMenu_HelpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMenu_HelpMenu_About = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainMenu
            // 
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MainMenu_FileMenu,
            this.MainMenu_ViewMenu,
            this.MainMenu_HelpMenu});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(784, 24);
            this.MainMenu.TabIndex = 1;
            this.MainMenu.Text = "menuStrip1";
            // 
            // MainMenu_FileMenu
            // 
            this.MainMenu_FileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MainMenu_FileMenu_Exit});
            this.MainMenu_FileMenu.Name = "MainMenu_FileMenu";
            this.MainMenu_FileMenu.Size = new System.Drawing.Size(37, 20);
            this.MainMenu_FileMenu.Text = "&File";
            // 
            // MainMenu_FileMenu_Exit
            // 
            this.MainMenu_FileMenu_Exit.Image = global::NetRender.Properties.Resources.close;
            this.MainMenu_FileMenu_Exit.Name = "MainMenu_FileMenu_Exit";
            this.MainMenu_FileMenu_Exit.Size = new System.Drawing.Size(152, 22);
            this.MainMenu_FileMenu_Exit.Text = "&Exit";
            this.MainMenu_FileMenu_Exit.Click += new System.EventHandler(this.MainMenu_FileMenu_Exit_Click);
            // 
            // MainMenu_ViewMenu
            // 
            this.MainMenu_ViewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MainMenu_ViewMenu_Master,
            this.MainMenu_ViewMenu_Slave,
            this.MainMenu_ViewMenu_Seperator1,
            this.MainMenu_ViewMenu_Preferences});
            this.MainMenu_ViewMenu.Name = "MainMenu_ViewMenu";
            this.MainMenu_ViewMenu.Size = new System.Drawing.Size(44, 20);
            this.MainMenu_ViewMenu.Text = "&View";
            // 
            // MainMenu_ViewMenu_Master
            // 
            this.MainMenu_ViewMenu_Master.Image = global::NetRender.Properties.Resources.server;
            this.MainMenu_ViewMenu_Master.Name = "MainMenu_ViewMenu_Master";
            this.MainMenu_ViewMenu_Master.Size = new System.Drawing.Size(146, 22);
            this.MainMenu_ViewMenu_Master.Text = "New &Master...";
            this.MainMenu_ViewMenu_Master.Click += new System.EventHandler(this.MainMenu_ViewMenu_Master_Click);
            // 
            // MainMenu_ViewMenu_Slave
            // 
            this.MainMenu_ViewMenu_Slave.Image = global::NetRender.Properties.Resources.blender;
            this.MainMenu_ViewMenu_Slave.Name = "MainMenu_ViewMenu_Slave";
            this.MainMenu_ViewMenu_Slave.Size = new System.Drawing.Size(146, 22);
            this.MainMenu_ViewMenu_Slave.Text = "New &Slave...";
            this.MainMenu_ViewMenu_Slave.Click += new System.EventHandler(this.MainMenu_ViewMenu_Slave_Click);
            // 
            // MainMenu_ViewMenu_Seperator1
            // 
            this.MainMenu_ViewMenu_Seperator1.Name = "MainMenu_ViewMenu_Seperator1";
            this.MainMenu_ViewMenu_Seperator1.Size = new System.Drawing.Size(143, 6);
            // 
            // MainMenu_ViewMenu_Preferences
            // 
            this.MainMenu_ViewMenu_Preferences.Image = global::NetRender.Properties.Resources.configure;
            this.MainMenu_ViewMenu_Preferences.Name = "MainMenu_ViewMenu_Preferences";
            this.MainMenu_ViewMenu_Preferences.Size = new System.Drawing.Size(146, 22);
            this.MainMenu_ViewMenu_Preferences.Text = "&Preferences...";
            this.MainMenu_ViewMenu_Preferences.Click += new System.EventHandler(this.MainMenu_ViewMenu_Preferences_Click);
            // 
            // MainMenu_HelpMenu
            // 
            this.MainMenu_HelpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MainMenu_HelpMenu_About});
            this.MainMenu_HelpMenu.Name = "MainMenu_HelpMenu";
            this.MainMenu_HelpMenu.Size = new System.Drawing.Size(44, 20);
            this.MainMenu_HelpMenu.Text = "&Help";
            // 
            // MainMenu_HelpMenu_About
            // 
            this.MainMenu_HelpMenu_About.Image = global::NetRender.Properties.Resources.notice;
            this.MainMenu_HelpMenu_About.Name = "MainMenu_HelpMenu_About";
            this.MainMenu_HelpMenu_About.Size = new System.Drawing.Size(116, 22);
            this.MainMenu_HelpMenu_About.Text = "&About...";
            this.MainMenu_HelpMenu_About.Click += new System.EventHandler(this.MainMenu_HelpMenu_About_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 462);
            this.Controls.Add(this.MainMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.MainMenu;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Blender NetRender";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem MainMenu_FileMenu;
        private System.Windows.Forms.ToolStripMenuItem MainMenu_FileMenu_Exit;
        private System.Windows.Forms.ToolStripMenuItem MainMenu_HelpMenu;
        private System.Windows.Forms.ToolStripMenuItem MainMenu_HelpMenu_About;
        private System.Windows.Forms.ToolStripMenuItem MainMenu_ViewMenu;
        private System.Windows.Forms.ToolStripMenuItem MainMenu_ViewMenu_Master;
        private System.Windows.Forms.ToolStripMenuItem MainMenu_ViewMenu_Slave;
        private System.Windows.Forms.ToolStripSeparator MainMenu_ViewMenu_Seperator1;
        private System.Windows.Forms.ToolStripMenuItem MainMenu_ViewMenu_Preferences;
    }
}

