﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetRender
{
    class Blender
    {
        /// <summary>
        /// The blender executable (full path).
        /// </summary>
        private string m_Executable;

        /// <summary>
        /// The blender process once it's executing.
        /// </summary>
        private Process m_Process;
        /// <summary>
        /// The blender file.
        /// </summary>
        private string m_BlendFile;
        /// <summary>
        /// The blender temp path.
        /// </summary>
        private string m_OutputDirectory;
        /// <summary>
        /// The frame to start rendering at in Blender.
        /// </summary>
        public int FrameStart = 1;
        /// <summary>
        /// The frame to stop rendering at in Blender. -1 if it's a single frame.
        /// </summary>
        public int FrameEnd = -1;
        /// <summary>
        /// The output format the files should be in. TGA IRIS HAMX FTYPE JPEG MOVIE IRIZ RAWTGA AVIRAW AVIJPEG PNG BMP FRAMESERVER HDR TIFF EXR MPEG AVICODEC QUICKTIME CINEON DPX
        /// </summary>
        public string OutputFormat = "PNG";

        /// <summary>
        /// Creates a new Blender wrapper to set up a command.
        /// </summary>
        public Blender()
        {
            m_Executable = Config.BlenderExecutable;
            m_OutputDirectory = Config.BlenderTempPath;
        }

        /// <summary>
        /// Executes Blender with the properties set in this class.
        /// </summary>
        public void Execute()
        {
            ProcessStartInfo info = new ProcessStartInfo();
            info.FileName = m_Executable;
            info.Arguments = "";

            // Hide the blender command window.
            info.UseShellExecute = false;
            info.CreateNoWindow = true;
            
            // The blend file.
            if (FrameEnd == -1)
            {
                info.Arguments += "-b \"" + m_BlendFile + "\" -o \"" + m_OutputDirectory + "\"/ -F " + OutputFormat + " -x 1 -f " + FrameStart.ToString();
            }
            else
            {
                info.Arguments += "-b \"" + m_BlendFile + "\" -o \"" + m_OutputDirectory + "\"/ -F " + OutputFormat + " -x 1 -s " + FrameStart.ToString() + " -e " + FrameEnd.ToString() + " -a";
            }
            
            m_Process = new Process();
            m_Process.StartInfo = info;
            m_Process.EnableRaisingEvents = true;
            m_Process.Start();

            m_Process.Exited += Blender_Exited;
        }

        /// <summary>
        /// The blend file that will be rendered.
        /// </summary>
        public string BlendFile
        {
            get { return m_BlendFile; }
            set { m_BlendFile = value; }
        }
        /// <summary>
        /// The output directory of the rendered frames.
        /// </summary>
        public string OutputDirectory
        {
            get { return m_OutputDirectory; }
            set { m_OutputDirectory = value; }
        }
        /// <summary>
        /// Called when Blender exits.
        /// </summary>
        public event EventHandler OnExited;

        /// <summary>
        /// Called when Blender exits.
        /// </summary>
        private void Blender_Exited(object sender, EventArgs e)
        {
            // call our own event handler if available.
            if (OnExited != null)
                OnExited(this, e);

            // clean up resources.
            m_Process = null;
        }
    }
}
