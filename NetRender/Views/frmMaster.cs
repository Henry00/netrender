﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NetRender.Common;
using NetRender.Extensions;
using System.IO;
using NetRender.Protocol;
using System.IO.Compression;

namespace NetRender.Views
{
    public partial class frmMaster : Form
    {
        public frmMaster()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Unique name to identify this master server by.
        /// </summary>
        private string m_Name;

        /// <summary>
        /// Unique name to identify this master server by.
        /// </summary>
        public string MasterName
        {
            get { return m_Name; }
        }

        /// <summary>
        /// The webserver.
        /// </summary>
        private MasterHttpServer m_HttpServer;

        /// <summary>
        /// List of all jobs currently handled on this master server.
        /// </summary>
        private Dictionary<string, MasterJob> m_Jobs = new Dictionary<string, MasterJob>();

        /// <summary>
        /// List of all the available slaves.
        /// </summary>
        private Dictionary<string, Slave> m_Slaves = new Dictionary<string, Slave>();

        /// <summary>
        /// Initialize all the required resources for this master server.
        /// </summary>
        private void frmMaster_Load(object sender, EventArgs e)
        {
            // generate a unique name for this master server.
            m_Name = Environment.MachineName + "-" + Guid.NewGuid().ToString().Replace("-", "");

            // display name in window title.
            Text += " " + m_Name;

            // called once the window has been shown to the user.
            // once the window has been shown, maximize it.
            // this cannot be done earlier due to a graphics glitch.
            Shown += delegate(object o1, EventArgs e1) {
                WindowState = FormWindowState.Maximized;
            };

            // once the window has closed, kill the http server.
            FormClosed += delegate (object o2, FormClosedEventArgs e2) {
                // can be null when the user didn't start listening yet.
                if (m_HttpServer != null)
                    m_HttpServer.StopListening();
            };
        }

        /// <summary>
        /// Start listening on the address and port then deactivate the button.
        /// </summary>
        private void Button_StartListening_Click(object sender, EventArgs e)
        {
            // create the http server.
            string address = TextBox_NetworkSettings_Address.Text;
            int port = (int)TextBox_NetworkSettings_Port.Value;
            m_HttpServer = new MasterHttpServer(address, port);
            m_HttpServer.ProcessRequest += HttpServer_ProcessRequest;

            // disable the start listening button.
            Button_StartListening.Enabled = false;

            // begin listening.
            m_HttpServer.StartListening();
        }

        /// <summary>
        /// Handle a request from the webserver on the main thread.
        /// </summary>
        private void HttpServer_ProcessRequest(object sender, MasterHttpServer.ProcessRequestEventArgs e)
        {
            string path = e.Context.Request.Url.AbsolutePath;

            /////////////////////////////////////////////////////////////////////////////////////////
            // HANDLE CLIENT REQUESTS
            /////////////////////////////////////////////////////////////////////////////////////////
            if (path.StartsWith("/job_"))
            {
                // find the start frame and end frame from the request path.
                string[] parts = path.Replace("/job_", "").Split('&');
                int frame_start = Convert.ToInt32(parts[0]);
                int frame_end = Convert.ToInt32(parts[1]);
                string output_format = parts[2];

                // save the zip file.
                string jobname = "job_" + Guid.NewGuid().ToString().Replace("-","");
                FileStream zipfile = new FileStream(Config.BlenderTempPath + "/" + jobname + ".zip", FileMode.CreateNew);
                e.Context.Request.InputStream.CopyTo(zipfile);
                zipfile.Close();

                // add the new job.
                m_Jobs[jobname] = new MasterJob(jobname, frame_start, frame_end, output_format, DateTime.Now);
            }

            /////////////////////////////////////////////////////////////////////////////////////////
            // HANDLE SLAVE REQUESTS
            /////////////////////////////////////////////////////////////////////////////////////////
            switch (e.Context.Request.Url.AbsolutePath)
            {
                // RECEIVE STATUS OF A RENDERING SLAVE.
                case "/status":
                    // read the POST content.
                    string body = new StreamReader(e.Context.Request.InputStream).ReadToEnd();
                    // deserialize xml to a slave instance.
                    Slave slave = SerializingUtils.DeserializeObject<Slave>(body);
                    // replace the slave information we have with the new information.
                    m_Slaves[slave.Name] = slave;
                    // update the last seen time.
                    m_Slaves[slave.Name].LastSeen = DateTime.Now.ToString("hh\\:mm\\:ss");

                    // slave is idle and ready for an assignment.
                    if (slave.Status == Slave.STATUS_IDLE)
                    {
                        // iterate through all jobs.
                        foreach (KeyValuePair<string,MasterJob> mjob in m_Jobs)
                        {
                            int start = 0;
                            int end = 0;
                            // find one or more unrendered frames.
                            if (mjob.Value.GetUnrenderedFrames(out start, out end, mjob.Value.FrameStart))
                            {
                                // create render job for the slave.
                                ServerSlaveWrapper wrapper = new ServerSlaveWrapper() {
                                    Type = ServerSlaveWrapper.TYPE_RENDERJOB,
                                    RenderJob = new ServerSlaveRenderJob(mjob.Value.JobName, start, end, mjob.Value.OutputFormat)
                                };
                                // send the job to the slave.
                                e.Context.Response.OutputStream.WriteString(SerializingUtils.SerializeObject(wrapper));
                                // mark frames as rendering on a slave.
                                if (end == -1)
                                    m_Jobs[mjob.Value.JobName].Frames[0].Status = MasterFrame.STATUS_RENDERING;
                                else
                                    // mark animation frames as rendered.
                                    for (int i = start; i <= end; i++)
                                        m_Jobs[mjob.Value.JobName].Frames[i - 1].Status = MasterFrame.STATUS_RENDERING;
                            }
                        }
                    }
                    else
                    {
                        // send an empty job to the slave.
                        e.Context.Response.OutputStream.WriteString(SerializingUtils.SerializeObject(new ServerSlaveWrapper() { Type = ServerSlaveWrapper.TYPE_NONE }));
                    }

                    e.Context.Response.StatusCode = 200;
                    e.Context.Response.OutputStream.Close();
                    return;

                // RECEIVE REQUEST FOR A FILE TRANSFER.
                case "/requestfile":
                    // read the POST content.
                    body = new StreamReader(e.Context.Request.InputStream).ReadToEnd();
                    // deserialize xml to a file request instance.
                    SlaveServerRequestFile request = SerializingUtils.DeserializeObject<SlaveServerRequestFile>(body);
                    // check which file it wants:
                    switch (request.FileName)
                    {
                        case SlaveServerRequestFile.FILE_JOB_ZIP:
                            // send the file to the slave.
                            ServerSlaveTransferFile transferFile = new ServerSlaveTransferFile()
                            {
                                JobName = request.JobName,
                                FileName = SlaveServerRequestFile.FILE_JOB_ZIP,
                                FileBytes = File.ReadAllBytes(Config.BlenderTempPath + "/" + request.JobName + ".zip")
                            };
                            // send the job to the slave.
                            e.Context.Response.OutputStream.WriteString(SerializingUtils.SerializeObject(transferFile));
                            break;
                    }

                    e.Context.Response.StatusCode = 200;
                    e.Context.Response.OutputStream.Close();
                    return;

                case "/renderresults":
                    // read the POST content.
                    body = new StreamReader(e.Context.Request.InputStream).ReadToEnd();
                    // deserialize xml to a file request instance.
                    SlaveServerRenderResults results = SerializingUtils.DeserializeObject<SlaveServerRenderResults>(body);
                    // save the file to the disk.
                    File.WriteAllBytes(Config.BlenderTempPath + "/" + results.JobName + ".results.zip", results.FileBytes);

                    // extract the zip file into the results directory of the job.
                    Directory.CreateDirectory(Config.BlenderTempPath + "/" + results.JobName + "_results");
                    using (ZipArchive archive = ZipFile.OpenRead(Config.BlenderTempPath + "/" + results.JobName + ".results.zip"))
                    {
                        archive.ExtractToDirectory(Config.BlenderTempPath + "/" + results.JobName + "_results", true);
                    }

                    // delete the zip file.
                    File.Delete(Config.BlenderTempPath + "/" + results.JobName + ".results.zip");

                    // mark frame as rendered.
                    if (results.FrameEnd == -1)
                        m_Jobs[results.JobName].Frames[0].Status = MasterFrame.STATUS_RENDERED;
                    else
                        // mark animation frames as rendered.
                        for (int i = results.FrameStart; i <= results.FrameEnd; i++)
                            m_Jobs[results.JobName].Frames[i-1].Status = MasterFrame.STATUS_RENDERED;

                    e.Context.Response.StatusCode = 200;
                    e.Context.Response.OutputStream.Close();
                    return;
            }

            /////////////////////////////////////////////////////////////////////////////////////////
            // HANDLE WEBSITE REQUESTS
            /////////////////////////////////////////////////////////////////////////////////////////
            string html = "";

            switch (e.Context.Request.Url.AbsolutePath)
            {
                case "/":
                    e.Context.Response.ContentType = "text/html";
                    e.Context.Response.OutputStream.WriteString(File.ReadAllText("html/index.html"));
                    break;
                case "/CSS/stylesheet.css":
                    e.Context.Response.ContentType = "text/css";
                    e.Context.Response.OutputStream.WriteString(File.ReadAllText("html/CSS/stylesheet.css"));
                    break;

                // TABLE OF ACTIVE JOBS ON THE MASTER SERVER.
                case "/jobs":
                    e.Context.Response.ContentType = "text/html";
                    // load job template:
                    string job_template = File.ReadAllText("html/job.html");

                    // add each job to the output html.
                    foreach (KeyValuePair<string,MasterJob> mjob in m_Jobs)
                    {
                        string newjob = job_template;
                        newjob = newjob.Replace("{!Name!}", mjob.Value.JobName);
                        newjob = newjob.Replace("{!Frames!}", mjob.Value.FrameEnd == -1 ? mjob.Value.FrameStart.ToString() : mjob.Value.FrameStart.ToString() + " - " + mjob.Value.FrameEnd.ToString());
                        newjob = newjob.Replace("{!Dispatched!}", mjob.Value.FramesDispatchedCount.ToString());
                        newjob = newjob.Replace("{!Done!}", mjob.Value.FramesDoneCount.ToString());
                        newjob = newjob.Replace("{!Started!}", mjob.Value.StartTime.ToString());
                        newjob = newjob.Replace("{!Lifetime!}", mjob.Value.Lifetime);
                        newjob = newjob.Replace("{!Format!}", mjob.Value.OutputFormat);
                        html += newjob;
                    }
                    // send to client.
                    e.Context.Response.OutputStream.WriteString(html);
                    break;

                // TABLE OF SLAVES REGISTERED ON THE MASTER SERVER.
                case "/slaves":
                    e.Context.Response.ContentType = "text/html";
                    // load job template:
                    string slave_template = File.ReadAllText("html/slave.html");

                    // add each job to the output html.
                    foreach (KeyValuePair<string, Slave> slave in m_Slaves)
                    {
                        string newslave = slave_template;
                        newslave = newslave.Replace("{!Name!}", slave.Value.Name);
                        newslave = newslave.Replace("{!File!}", slave.Value.BlendFile);
                        newslave = newslave.Replace("{!Status!}", slave.Value.Status);
                        newslave = newslave.Replace("{!Seen!}", slave.Value.LastSeen);
                        if (slave.Value.Status == Slave.STATUS_IDLE) {
                            newslave = newslave.Replace("{!Job!}", "None");
                            newslave = newslave.Replace("{!Frames!}", "None");
                        }
                        else
                        {
                            newslave = newslave.Replace("{!Job!}", slave.Value.JobName);
                            newslave = newslave.Replace("{!Frames!}", slave.Value.FrameEnd == -1 ? slave.Value.FrameStart.ToString() : slave.Value.FrameStart.ToString() + " - " + slave.Value.FrameEnd.ToString());
                        }
                        
                        html += newslave;
                    }
                    // send to client.
                    e.Context.Response.OutputStream.WriteString(html);
                    break;

                default:
                    e.Context.Response.ContentType = "text/html";
                    e.Context.Response.OutputStream.WriteString("PAGE NOT FOUND");
                    break;
            }

            e.Context.Response.StatusCode = 200;
            e.Context.Response.OutputStream.Close();
        }

    }
}
