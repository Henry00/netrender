﻿namespace NetRender.Views
{
    partial class frmSlave
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSlave));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.PictureBox_Preview = new System.Windows.Forms.PictureBox();
            this.GroupBox_NetworkSettings = new System.Windows.Forms.GroupBox();
            this.Button_StartWorking = new System.Windows.Forms.Button();
            this.TextBox_NetworkSettings_Port = new System.Windows.Forms.NumericUpDown();
            this.Label_NetworkSettings_Port = new System.Windows.Forms.Label();
            this.TextBox_NetworkSettings_Address = new System.Windows.Forms.TextBox();
            this.Label_NetworkSettings_Address = new System.Windows.Forms.Label();
            this.StatusBar = new System.Windows.Forms.StatusStrip();
            this.StatusBar_Activity = new System.Windows.Forms.ToolStripStatusLabel();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Preview)).BeginInit();
            this.GroupBox_NetworkSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox_NetworkSettings_Port)).BeginInit();
            this.StatusBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 249F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.PictureBox_Preview, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.GroupBox_NetworkSettings, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 133F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(584, 339);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // PictureBox_Preview
            // 
            this.PictureBox_Preview.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanel1.SetColumnSpan(this.PictureBox_Preview, 2);
            this.PictureBox_Preview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PictureBox_Preview.Location = new System.Drawing.Point(4, 137);
            this.PictureBox_Preview.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBox_Preview.Name = "PictureBox_Preview";
            this.PictureBox_Preview.Size = new System.Drawing.Size(576, 198);
            this.PictureBox_Preview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox_Preview.TabIndex = 0;
            this.PictureBox_Preview.TabStop = false;
            // 
            // GroupBox_NetworkSettings
            // 
            this.GroupBox_NetworkSettings.Controls.Add(this.Button_StartWorking);
            this.GroupBox_NetworkSettings.Controls.Add(this.TextBox_NetworkSettings_Port);
            this.GroupBox_NetworkSettings.Controls.Add(this.Label_NetworkSettings_Port);
            this.GroupBox_NetworkSettings.Controls.Add(this.TextBox_NetworkSettings_Address);
            this.GroupBox_NetworkSettings.Controls.Add(this.Label_NetworkSettings_Address);
            this.GroupBox_NetworkSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GroupBox_NetworkSettings.Location = new System.Drawing.Point(3, 3);
            this.GroupBox_NetworkSettings.Name = "GroupBox_NetworkSettings";
            this.GroupBox_NetworkSettings.Size = new System.Drawing.Size(243, 127);
            this.GroupBox_NetworkSettings.TabIndex = 1;
            this.GroupBox_NetworkSettings.TabStop = false;
            this.GroupBox_NetworkSettings.Text = "Network Settings";
            // 
            // Button_StartWorking
            // 
            this.Button_StartWorking.Location = new System.Drawing.Point(12, 92);
            this.Button_StartWorking.Name = "Button_StartWorking";
            this.Button_StartWorking.Size = new System.Drawing.Size(218, 27);
            this.Button_StartWorking.TabIndex = 4;
            this.Button_StartWorking.Text = "Start Working";
            this.Button_StartWorking.UseVisualStyleBackColor = true;
            this.Button_StartWorking.Click += new System.EventHandler(this.Button_StartWorking_Click);
            // 
            // TextBox_NetworkSettings_Port
            // 
            this.TextBox_NetworkSettings_Port.Location = new System.Drawing.Point(81, 54);
            this.TextBox_NetworkSettings_Port.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.TextBox_NetworkSettings_Port.Name = "TextBox_NetworkSettings_Port";
            this.TextBox_NetworkSettings_Port.Size = new System.Drawing.Size(82, 23);
            this.TextBox_NetworkSettings_Port.TabIndex = 3;
            this.TextBox_NetworkSettings_Port.Value = new decimal(new int[] {
            8000,
            0,
            0,
            0});
            // 
            // Label_NetworkSettings_Port
            // 
            this.Label_NetworkSettings_Port.AutoSize = true;
            this.Label_NetworkSettings_Port.Location = new System.Drawing.Point(34, 56);
            this.Label_NetworkSettings_Port.Name = "Label_NetworkSettings_Port";
            this.Label_NetworkSettings_Port.Size = new System.Drawing.Size(41, 16);
            this.Label_NetworkSettings_Port.TabIndex = 2;
            this.Label_NetworkSettings_Port.Text = "Port:";
            // 
            // TextBox_NetworkSettings_Address
            // 
            this.TextBox_NetworkSettings_Address.Location = new System.Drawing.Point(81, 25);
            this.TextBox_NetworkSettings_Address.Name = "TextBox_NetworkSettings_Address";
            this.TextBox_NetworkSettings_Address.Size = new System.Drawing.Size(149, 23);
            this.TextBox_NetworkSettings_Address.TabIndex = 1;
            this.TextBox_NetworkSettings_Address.Text = "127.0.0.1";
            // 
            // Label_NetworkSettings_Address
            // 
            this.Label_NetworkSettings_Address.AutoSize = true;
            this.Label_NetworkSettings_Address.Location = new System.Drawing.Point(9, 28);
            this.Label_NetworkSettings_Address.Name = "Label_NetworkSettings_Address";
            this.Label_NetworkSettings_Address.Size = new System.Drawing.Size(66, 16);
            this.Label_NetworkSettings_Address.TabIndex = 0;
            this.Label_NetworkSettings_Address.Text = "Address:";
            // 
            // StatusBar
            // 
            this.StatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusBar_Activity});
            this.StatusBar.Location = new System.Drawing.Point(0, 339);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.StatusBar.Size = new System.Drawing.Size(584, 22);
            this.StatusBar.TabIndex = 2;
            this.StatusBar.Text = "statusStrip1";
            // 
            // StatusBar_Activity
            // 
            this.StatusBar_Activity.Name = "StatusBar_Activity";
            this.StatusBar_Activity.Size = new System.Drawing.Size(10, 17);
            this.StatusBar_Activity.Text = " ";
            // 
            // frmSlave
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.StatusBar);
            this.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(400, 300);
            this.Name = "frmSlave";
            this.Text = "Slave";
            this.Load += new System.EventHandler(this.frmSlave_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Preview)).EndInit();
            this.GroupBox_NetworkSettings.ResumeLayout(false);
            this.GroupBox_NetworkSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox_NetworkSettings_Port)).EndInit();
            this.StatusBar.ResumeLayout(false);
            this.StatusBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox PictureBox_Preview;
        private System.Windows.Forms.GroupBox GroupBox_NetworkSettings;
        private System.Windows.Forms.Button Button_StartWorking;
        private System.Windows.Forms.NumericUpDown TextBox_NetworkSettings_Port;
        private System.Windows.Forms.Label Label_NetworkSettings_Port;
        private System.Windows.Forms.TextBox TextBox_NetworkSettings_Address;
        private System.Windows.Forms.Label Label_NetworkSettings_Address;
        private System.Windows.Forms.StatusStrip StatusBar;
        private System.Windows.Forms.ToolStripStatusLabel StatusBar_Activity;
    }
}