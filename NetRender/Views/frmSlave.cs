﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NetRender.Common;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Xml.Serialization;
using NetRender.Protocol;
using System.IO.Compression;
using NetRender.Extensions;

namespace NetRender.Views
{
    public partial class frmSlave : Form
    {
        public frmSlave()
        {
            InitializeComponent();
        }

        /// <summary>
        /// All the information about this slave. This is shared with the master server.
        /// </summary>
        private Slave m_Slave;
        /// <summary>
        /// Timer to regularily negotiate with the master server.
        /// </summary>
        private Timer m_Timer;
        /// <summary>
        /// WebClient to HTTP connect with the master server.
        /// </summary>
        private WebClient m_Client;
        /// <summary>
        /// The Blender process that is rendering.
        /// </summary>
        private Blender m_Blender;

        /// <summary>
        /// Initialize all the required resources for this slave.
        /// </summary>
        private void frmSlave_Load(object sender, EventArgs e)
        {
            // create the slave representation and generate a unique slave name.
            m_Slave = new Slave(Environment.MachineName + "-" + Guid.NewGuid().ToString().Replace("-", ""));

            // display name in window title.
            Text += " " + m_Slave.Name;

            // create the webclient.
            m_Client = new WebClient();

            // create the timer.
            m_Timer = new Timer();
            m_Timer.Interval = 2000;
            m_Timer.Tick += M_Timer_Tick;

            // called once the window has been shown to the user.
            // once the window has been shown, maximize it.
            // this cannot be done earlier due to a graphics glitch.
            Shown += delegate (object o1, EventArgs e1) {
                WindowState = FormWindowState.Maximized;
            };
        }

        /// <summary>
        /// Start negotiating with the master server on the address and port then deactivate the button.
        /// </summary>
        private void Button_StartWorking_Click(object sender, EventArgs e)
        {
            // configure the webclient.
            m_Client.BaseAddress = "http://" + TextBox_NetworkSettings_Address.Text + ":" + TextBox_NetworkSettings_Port.Value.ToString();

            // disable the start listening button.
            Button_StartWorking.Enabled = false;

            // start the timer.
            m_Timer.Start();

            // set the initial state of the slave.
            m_Slave.Status = Slave.STATUS_IDLE;

            UpdateStatus();
        }

        /// <summary>
        /// Send our information to the master server and receive work if available.
        /// </summary>
        private void M_Timer_Tick(object sender, EventArgs e)
        {
            // upload our information to the master server and read the reply.
            ServerSlaveWrapper wrapper;
            try
            {
                wrapper = SerializingUtils.DeserializeObject<ServerSlaveWrapper>(
                    m_Client.UploadString("/status", SerializingUtils.SerializeObject(m_Slave))
                );
            }
            catch (Exception) { return; } // timeout exception or deserialization error.

            /////////////////////////////////////////////////////////////////////////////////////////////
            // STATE MACHINE - Depending on the state, do various types of work.
            /////////////////////////////////////////////////////////////////////////////////////////////
            switch (m_Slave.Status)
            {
                /////////////////////////////////////////////////////////////////////////////////////////////
                // STATE: IDLE - Receive a new assignment from the master server if available.
                /////////////////////////////////////////////////////////////////////////////////////////////
                case Slave.STATUS_IDLE:

                    // reset information.
                    m_Slave.BlendFile = "Unknown";

                    // check assignment from the master server.
                    switch (wrapper.Type)
                    {
                        /////////////////////////////////////////////////////////////////////////////////////////////
                        // TASK: NONE - No work is available, remain idle.
                        /////////////////////////////////////////////////////////////////////////////////////////////
                        case ServerSlaveWrapper.TYPE_NONE:
                            break;

                        /////////////////////////////////////////////////////////////////////////////////////////////
                        // TASK: RENDER JOB - Start a new rendering job.
                        /////////////////////////////////////////////////////////////////////////////////////////////
                        case ServerSlaveWrapper.TYPE_RENDERJOB:
                            // store rendering job information.
                            m_Slave.JobName = wrapper.RenderJob.JobName;
                            m_Slave.FrameStart = wrapper.RenderJob.FrameStart;
                            m_Slave.FrameEnd = wrapper.RenderJob.FrameEnd;
                            m_Slave.OutputFormat = wrapper.RenderJob.OutputFormat;
                            // switch status -> starting.
                            m_Slave.Status = Slave.STATUS_STARTING;
                            break;
                    }
                    break;

                /////////////////////////////////////////////////////////////////////////////////////////////
                // STATE: STARTING - Ensure we have everything we need to begin rendering.
                /////////////////////////////////////////////////////////////////////////////////////////////
                case Slave.STATUS_STARTING:
                    /////////////////////////////////////////////////////////////////////////////////////////////
                    // WORK: DOWNLOAD ZIP FILE - Download the zip file for the .blend and project files.
                    /////////////////////////////////////////////////////////////////////////////////////////////
                    if (!Directory.Exists(ZipFileDirectory))
                    {
                        try
                        {
                            // upload a file request to the master server and download the file.
                            ServerSlaveTransferFile file = SerializingUtils.DeserializeObject<ServerSlaveTransferFile>(
                                m_Client.UploadString("/requestfile", SerializingUtils.SerializeObject(
                                    new SlaveServerRequestFile() {
                                        JobName = m_Slave.JobName,
                                        FileName = SlaveServerRequestFile.FILE_JOB_ZIP
                                    }
                                ))
                            );
                            // create the file on the hard-drive and write the received contents into it.
                            File.WriteAllBytes(ZipFileName, file.FileBytes);

                            // extract the files from the zip file so that Blender can access them.
                            Directory.CreateDirectory(ZipFileDirectory);
                            using (ZipArchive archive = ZipFile.OpenRead(ZipFileName))
                            {
                                archive.ExtractToDirectory(ZipFileDirectory, true);
                            }

                            // delete the zip file as we no longer need it.
                            try
                            {
                                File.Delete(ZipFileName);
                            }
                            catch (Exception) { };

                            // switch status -> rendering.
                            m_Blender = null;
                            m_Slave.Status = Slave.STATUS_RENDERING;
                        }
                        catch (Exception) { return; } // timeout exception or deserialization error or unzip error.
                    }
                    else
                    {
                        // the directory already exists, remove it and start a clean new render.
                        Directory.Delete(ZipFileDirectory, true);
                    }
                    break;

                /////////////////////////////////////////////////////////////////////////////////////////////
                // STATE: RENDERING - Begin rendering by starting blender.
                /////////////////////////////////////////////////////////////////////////////////////////////
                case Slave.STATUS_RENDERING:
                    /////////////////////////////////////////////////////////////////////////////////////////////
                    // WORK: START BLENDER - Start Blender and have it render out the scene.
                    /////////////////////////////////////////////////////////////////////////////////////////////
                    if (m_Blender == null)
                    {
                        // create a new blender process wrapper.
                        m_Blender = new Blender();
                        m_Blender.BlendFile = BlenderFileName;
                        m_Slave.BlendFile = Path.GetFileName(m_Blender.BlendFile);
                        m_Blender.OutputDirectory = ZipFileDirectory + "_frames";
                        m_Blender.FrameStart = m_Slave.FrameStart;
                        m_Blender.FrameEnd = m_Slave.FrameEnd;
                        m_Blender.OutputFormat = m_Slave.OutputFormat;
                        Directory.CreateDirectory(m_Blender.OutputDirectory);

                        // set up render completion event.
                        m_Blender.OnExited += (object sender1, EventArgs e1) => {
                            // switch status -> uploading results.
                            m_Slave.Status = Slave.STATUS_UPLOADING;
                        };

                        // start rendering.
                        m_Blender.Execute();
                    }
                    break;

                /////////////////////////////////////////////////////////////////////////////////////////////
                // STATE: UPLOADING - Send the rendered frames to the master server.
                /////////////////////////////////////////////////////////////////////////////////////////////
                case Slave.STATUS_UPLOADING:
                    try
                    {
                        // pack all of the rendered frames into a zip file.
                        ZipFile.CreateFromDirectory(ZipFileDirectory + "_frames", ZipFileDirectory + ".frames.zip", CompressionLevel.Optimal, false);

                        // send the rendered frames zip file to the master server.
                        try {
                            m_Client.UploadString("/renderresults", SerializingUtils.SerializeObject(new SlaveServerRenderResults() {
                                    FrameStart = m_Slave.FrameStart,
                                    FrameEnd = m_Slave.FrameEnd,
                                    JobName = m_Slave.JobName,
                                    FileBytes = File.ReadAllBytes(ZipFileDirectory + ".frames.zip")
                                })
                            );
                        }
                        catch (Exception) { } // timeout exception or filesystem errors.

                        // try finding a frame, possibly the last file in the directory for preview in the viewport.
                        try
                        {
                            string preview = "";
                            foreach (string file in Directory.EnumerateFiles(ZipFileDirectory + "_frames"))
                                preview = file;
                            if (preview != "")
                                using (var bmpTemp = new Bitmap(preview))
                                {
                                    PictureBox_Preview.Image = new Bitmap(bmpTemp);
                                }
                        }
                        catch (Exception) { } // gdi errors or filesystem errors.

                        // delete the directory with the frames.
                        Directory.Delete(ZipFileDirectory + "_frames", true);

                        // delete the zip file.
                        File.Delete(ZipFileDirectory + ".frames.zip");
                    }
                    catch (Exception) { } // filesystem errors.

                    // we have to give up (if exceptions occured) so we are at least available again later.
                    // switch status -> idle.
                    m_Slave.Status = Slave.STATUS_IDLE;
                    break;
            }

            // update the user interface.
            UpdateStatus();
        }

        /// <summary>
        /// Update the user interface status to reflect the slave status.
        /// </summary>
        private void UpdateStatus()
        {
            StatusBar_Activity.Text = "Status: " + m_Slave.Status + ", ";

            if (m_Slave.Status == Slave.STATUS_IDLE)
            {
                StatusBar_Activity.Text += "Job: None, ";
                StatusBar_Activity.Text += "Frames: None";
            }
            else
            {
                StatusBar_Activity.Text += "Job: " + m_Slave.JobName + ", ";
                StatusBar_Activity.Text += "Frames: " + (m_Slave.FrameEnd == -1 ? m_Slave.FrameStart.ToString() : m_Slave.FrameStart.ToString() + " - " + m_Slave.FrameEnd.ToString());
            }
        }

        /// <summary>
        /// The full path to the temporary zip file name of the current job.
        /// </summary>
        public string ZipFileName
        {
            get { return Config.BlenderTempPath + "/" + m_Slave.JobName + ".slave.zip"; }
        }
        /// <summary>
        /// The full path to the temporary zip file directory of the current job.
        /// </summary>
        public string ZipFileDirectory
        {
            get { return Config.BlenderTempPath + "/" + m_Slave.JobName; }
        }
        /// <summary>
        /// The .blend file of the current job.
        /// </summary>
        public string BlenderFileName
        {
            get
            {
                foreach (string file in Directory.EnumerateFiles(ZipFileDirectory))
                    if (file.EndsWith(".blend"))
                        return file;
                return "";
            }
        }
    }
}
