﻿namespace NetRender.Views
{
    partial class frmPreferences
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPreferences));
            this.ButtonSave = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBox_BlenderExecutable = new System.Windows.Forms.TextBox();
            this.ButtonSelectBlenderExecutable = new System.Windows.Forms.Button();
            this.ButtonCancel = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBox_BlenderTempPath = new System.Windows.Forms.TextBox();
            this.ButtonSelectBlenderTempPath = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ButtonSave
            // 
            this.ButtonSave.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonSave.Location = new System.Drawing.Point(503, 148);
            this.ButtonSave.Name = "ButtonSave";
            this.ButtonSave.Size = new System.Drawing.Size(137, 30);
            this.ButtonSave.TabIndex = 0;
            this.ButtonSave.Text = "Save";
            this.ButtonSave.UseVisualStyleBackColor = true;
            this.ButtonSave.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Blender Executable:";
            // 
            // TextBox_BlenderExecutable
            // 
            this.TextBox_BlenderExecutable.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_BlenderExecutable.Location = new System.Drawing.Point(157, 6);
            this.TextBox_BlenderExecutable.Name = "TextBox_BlenderExecutable";
            this.TextBox_BlenderExecutable.Size = new System.Drawing.Size(415, 23);
            this.TextBox_BlenderExecutable.TabIndex = 2;
            // 
            // ButtonSelectBlenderExecutable
            // 
            this.ButtonSelectBlenderExecutable.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonSelectBlenderExecutable.Location = new System.Drawing.Point(578, 5);
            this.ButtonSelectBlenderExecutable.Name = "ButtonSelectBlenderExecutable";
            this.ButtonSelectBlenderExecutable.Size = new System.Drawing.Size(62, 25);
            this.ButtonSelectBlenderExecutable.TabIndex = 3;
            this.ButtonSelectBlenderExecutable.Text = "...";
            this.ButtonSelectBlenderExecutable.UseVisualStyleBackColor = true;
            this.ButtonSelectBlenderExecutable.Click += new System.EventHandler(this.Button_SelectBlenderExecutable_Click);
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonCancel.Location = new System.Drawing.Point(12, 148);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(137, 30);
            this.ButtonCancel.TabIndex = 4;
            this.ButtonCancel.Text = "Cancel";
            this.ButtonCancel.UseVisualStyleBackColor = true;
            this.ButtonCancel.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Blender Temp Path:";
            // 
            // TextBox_BlenderTempPath
            // 
            this.TextBox_BlenderTempPath.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_BlenderTempPath.Location = new System.Drawing.Point(157, 35);
            this.TextBox_BlenderTempPath.Name = "TextBox_BlenderTempPath";
            this.TextBox_BlenderTempPath.Size = new System.Drawing.Size(415, 23);
            this.TextBox_BlenderTempPath.TabIndex = 2;
            // 
            // ButtonSelectBlenderTempPath
            // 
            this.ButtonSelectBlenderTempPath.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.ButtonSelectBlenderTempPath.Location = new System.Drawing.Point(578, 35);
            this.ButtonSelectBlenderTempPath.Name = "ButtonSelectBlenderTempPath";
            this.ButtonSelectBlenderTempPath.Size = new System.Drawing.Size(62, 23);
            this.ButtonSelectBlenderTempPath.TabIndex = 5;
            this.ButtonSelectBlenderTempPath.Text = "...";
            this.ButtonSelectBlenderTempPath.UseVisualStyleBackColor = true;
            this.ButtonSelectBlenderTempPath.Click += new System.EventHandler(this.ButtonSelectBlenderTempPath_Click);
            // 
            // frmPreferences
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 190);
            this.Controls.Add(this.ButtonSelectBlenderTempPath);
            this.Controls.Add(this.ButtonCancel);
            this.Controls.Add(this.ButtonSelectBlenderExecutable);
            this.Controls.Add(this.TextBox_BlenderTempPath);
            this.Controls.Add(this.TextBox_BlenderExecutable);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ButtonSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPreferences";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Blender NetRender Preferences";
            this.Load += new System.EventHandler(this.frmPreferences_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBox_BlenderExecutable;
        private System.Windows.Forms.Button ButtonSelectBlenderExecutable;
        private System.Windows.Forms.Button ButtonCancel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBox_BlenderTempPath;
        private System.Windows.Forms.Button ButtonSelectBlenderTempPath;
    }
}