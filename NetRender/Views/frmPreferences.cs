﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetRender.Views
{
    public partial class frmPreferences : Form
    {
        public frmPreferences()
        {
            InitializeComponent();
        }

        /// <summary>
        /// On loading the preferences window, add the current settings to the controls.
        /// </summary>
        private void frmPreferences_Load(object sender, EventArgs e)
        {
            TextBox_BlenderExecutable.Text = Config.BlenderExecutable;
            TextBox_BlenderTempPath.Text = Config.BlenderTempPath;
        }

        /// <summary>
        /// Open a file selection window to select the blender executable.
        /// </summary>
        private void Button_SelectBlenderExecutable_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.CheckFileExists = true;
            dialog.Title = "Select Blender Executable...";
            dialog.Filter = "Executable Files (*.exe)|*.exe";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                TextBox_BlenderExecutable.Text = dialog.FileName.Replace('\\', '/');
            }
        }

        /// <summary>
        /// Open a directory selection window to select the blender temp path.
        /// </summary>
        private void ButtonSelectBlenderTempPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "Select the temporary path for Blender.";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                TextBox_BlenderTempPath.Text = dialog.SelectedPath.Replace('\\','/');
            }
        }

        /// <summary>
        /// Save the new user preferences to the config.xml file and close the window.
        /// </summary>
        private void SaveButton_Click(object sender, EventArgs e)
        {
            Config.BlenderExecutable = TextBox_BlenderExecutable.Text.Trim().Replace('\\','/');
            Config.BlenderTempPath = TextBox_BlenderTempPath.Text.Trim().Replace('\\', '/');
            Close();
        }

        /// <summary>
        /// Close the window without saving.
        /// </summary>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
