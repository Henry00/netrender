﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NetRender.Views;

namespace NetRender
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private frmPreferences m_Preferences;

        /// <summary>
        /// Startup routine for the program.
        /// </summary>
        private void frmMain_Load(object sender, EventArgs e)
        {
            m_Preferences = new frmPreferences();
            m_Preferences.Owner = this;
            m_Preferences.ShowDialog();
        }

        /// <summary>
        /// Display a short message for the about information.
        /// </summary>
        private void MainMenu_HelpMenu_About_Click(object sender, EventArgs e)
        {
            MessageBox.Show(this, "Blender NetRender" + Environment.NewLine + "Copyright © Henry de Jongh & Georg Meier 2016" + Environment.NewLine + Environment.NewLine + "https://bitbucket.org/Henry00/netrender", "About Blender NetRender", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Create a new master server window.
        /// </summary>
        private void MainMenu_ViewMenu_Master_Click(object sender, EventArgs e)
        {
            frmMaster master = new frmMaster();
            master.Owner = this;
            master.MdiParent = this;
            master.Show();
        }

        /// <summary>
        /// Create a new slave window.
        /// </summary>
        private void MainMenu_ViewMenu_Slave_Click(object sender, EventArgs e)
        {
            frmSlave slave = new frmSlave();
            slave.Owner = this;
            slave.MdiParent = this;
            slave.Show();
        }

        /// <summary>
        /// Show the preferences window.
        /// </summary>
        private void MainMenu_ViewMenu_Preferences_Click(object sender, EventArgs e)
        {
            m_Preferences.ShowDialog();
        }

        /// <summary>
        /// Exit the program by closing this main window.
        /// </summary>
        private void MainMenu_FileMenu_Exit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
