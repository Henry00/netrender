﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using NetRender.Extensions;

namespace NetRender
{
    /// <summary>
    /// Config loads the program configuration from an xml file and saves new data.
    /// </summary>
    static class Config
    {
        private static XDocument m_Document;

        private static XElement m_Root;

        private static XElement m_Blender;

        private static XElement m_BlenderTempPath;

        static Config()
        {
            // if there is no configuration, create a new file.
            if (!File.Exists("config.xml"))
                new XDocument(
                    new XElement("program",
                        new XAttribute("version", "0.1")
                    )
                ).Save("config.xml");

            // load the configuration file.
            m_Document = XDocument.Load("config.xml");
            m_Root = m_Document.Root;

            /**************************************************/
            /*                DEFAULT VALUES                  */
            /**************************************************/
            m_Blender = m_Root.AddDefault("blender", new XElement("blender"));
            m_BlenderTempPath = m_Root.AddDefault("temp", new XElement("temp"));
            /**************************************************/
        }

        /// <summary>
        /// Gets or sets the path of the Blender executable.
        /// </summary>
        public static string BlenderExecutable
        {
            get
            {
                return (string)m_Blender.Attribute("executable")?.Value;
            }
            set
            {
                m_Blender.SetAttributeValue("executable", value);
                m_Document.Save("config.xml");
            }
        }

        /// <summary>
        /// Gets or sets the path of the Blender temporary directory.
        /// </summary>
        public static string BlenderTempPath
        {
            get
            {
                return (string)m_BlenderTempPath.Attribute("path")?.Value;
            }
            set
            {
                m_BlenderTempPath.SetAttributeValue("path", value);
                m_Document.Save("config.xml");
            }
        }
    }
}
